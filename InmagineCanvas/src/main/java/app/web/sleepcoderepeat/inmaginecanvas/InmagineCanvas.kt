package app.web.sleepcoderepeat.inmaginecanvas

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toDrawable
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.filter.GPUImageFilterGroup
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGrayscaleFilter
import jp.co.cyberagent.android.gpuimage.filter.GPUImagePosterizeFilter
import kotlin.math.roundToInt


class InmagineCanvas : View {

    private var paint: Paint
    private val rectList: MutableList<RectCanvas> = arrayListOf()
    private var imageId: Int = 0
    var image: Drawable? = null
    private var parentWidth: Int = 0
    private var parentHeight: Int = 0
    private var mainParentSize: Int = 0
    private val parentPadding = 50

    private var imageFilter: GPUImage? = null

    var isDraw = false

    constructor(context: Context?) : super(context) {
        if (!isInEditMode) {
            imageFilter = GPUImage(context)
        }
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        if (!isInEditMode) {
            imageFilter = GPUImage(context)
        }
    }

    init {
        paint = Paint().apply {
            color = ContextCompat.getColor(context, R.color.borderRectangle)
            strokeWidth = 10F
            style = Paint.Style.STROKE
        }
        setBackgroundResource(R.color.canvasBackground)
        if (!isInEditMode) {
            setLayerType(LAYER_TYPE_HARDWARE, null)
        }
    }

    fun drawTwoTimesTwoRect() {
        isDraw = true
        rectList.clear()
        image = null

        val divider = 2
        val rows = 2
        val columns = 2
        val margin = parentPadding * 2
        val totalRectMargin = parentPadding * (divider + 1)
        val widthRect = (mainParentSize - totalRectMargin) / divider
        val heightRect = (mainParentSize - totalRectMargin) / divider
        val spaceBetweenWidth = parentWidth - margin.toFloat() - (widthRect * divider)
        val spaceBetweenHeight = parentHeight - margin.toFloat() - (heightRect * divider)

        process(
            rows,
            columns,
            widthRect,
            heightRect,
            spaceBetweenWidth,
            spaceBetweenHeight
        )
    }

    fun drawThreeTimesThreeRect() {
        isDraw = true
        rectList.clear()
        image = null

        val divider = 3
        val rows = 3
        val columns = 3
        val margin = parentPadding * 2
        val totalRectMargin = parentPadding * (divider + 1)
        val widthRect = (mainParentSize - totalRectMargin) / divider
        val heightRect = (mainParentSize - totalRectMargin) / divider
        val spaceBetweenWidth = (parentWidth - margin.toFloat() - (widthRect * divider)) / 2
        val spaceBetweenHeight = (parentHeight - margin.toFloat() - (heightRect * divider)) / 2
        process(
            rows,
            columns,
            widthRect,
            heightRect,
            spaceBetweenWidth,
            spaceBetweenHeight
        )
    }

    private fun process(
        rows: Int,
        columns: Int,
        widthRect: Int,
        heightRect: Int,
        spaceBetweenWidth: Float,
        spaceBetweenHeight: Float,
    ) {
        var tempLeftPosition: Float
        var tempTopPosition: Float
        var tempRightPosition = 0F
        var tempBottomPosition: Float
        for (row in 0 until rows) {
            for (column in 0 until columns) {
                tempLeftPosition =
                    if (column == 0) parentPadding.toFloat() else tempRightPosition + spaceBetweenWidth
                tempTopPosition =
                    (heightRect * row) + (spaceBetweenHeight * row) + parentPadding.toFloat()
                tempRightPosition = tempLeftPosition + widthRect
                tempBottomPosition = tempTopPosition + heightRect
                rectList.add(
                    RectCanvas(
                        left = tempLeftPosition,
                        top = tempTopPosition,
                        right = tempRightPosition,
                        bottom = tempBottomPosition,
                        paint
                    )
                )
            }
        }
        invalidate()
    }

    fun loadImage(drawableId: Int) {
        ResourcesCompat.getDrawable(resources, drawableId, null)?.let {
            image = it
            imageId = drawableId
        }
        invalidate()
    }

    fun appliedFilter() {
        imageFilter?.apply {
            image?.let {
                val groupFilter = GPUImageFilterGroup()
                groupFilter.addFilter(GPUImageGrayscaleFilter())
                groupFilter.addFilter(GPUImagePosterizeFilter())
                setFilter(groupFilter)
                setImage(BitmapFactory.decodeResource(resources, imageId))
                image = bitmapWithFilterApplied.toDrawable(resources)
                invalidate()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        parentHeight = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        parentWidth = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        mainParentSize = if (parentHeight < parentWidth) parentHeight else parentWidth
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.apply {
            Log.i("HW CANVAS INFO", canvas.isHardwareAccelerated.toString())

            for (item in rectList) {
                canvas.drawRect(item.left, item.top, item.right, item.bottom, item.paint)
            }
            image?.let {
                for (item in rectList) {
                    it.setBounds(
                        item.left.roundToInt(), item.top.roundToInt(),
                        item.right.roundToInt(), item.bottom.roundToInt()
                    )
                    it.draw(canvas)
                }
            }
        }
    }

    data class RectCanvas(
        val left: Float,
        val top: Float,
        val right: Float,
        val bottom: Float,
        val paint: Paint
    )
}