# Inmagine Submission Test

This app is building for finishing test recruitment at Inmagine.


### Import library
  1. Open **android studio project**
  2. Going to **> Project Structure > Modules > New Modules > Import Jar/.AAR Package**
  3. Browse file and select **InmagineCanvas-release.arr** (included in this root repository)
  4. Going to **build.gradle (module: app)**
  5. add `implementation project(":InmagineCanvas-release")` in **dependencies** section

##### Thanks reviewer

feel free to contact me at nandarm.sti@gmail.com