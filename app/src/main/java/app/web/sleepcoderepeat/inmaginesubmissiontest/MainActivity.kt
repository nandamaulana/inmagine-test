package app.web.sleepcoderepeat.inmaginesubmissiontest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_two_times_two.setOnClickListener {
            ic_canvas.drawTwoTimesTwoRect()
            btn_load_image.isEnabled = ic_canvas.isDraw
            btn_applied_filter.isEnabled = false
        }
        btn_three_times_three.setOnClickListener {
            ic_canvas.drawThreeTimesThreeRect()
            btn_load_image.isEnabled = ic_canvas.isDraw
            btn_applied_filter.isEnabled = false
        }
        btn_load_image.setOnClickListener {
            ic_canvas.loadImage(R.drawable.panorama)
            btn_applied_filter.isEnabled = true
        }
        btn_applied_filter.setOnClickListener {
            ic_canvas.image?.let {
                ic_canvas.appliedFilter()
            }
        }
    }
}